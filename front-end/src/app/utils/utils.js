export function parseUrl() {
//  debugger;
  const url = window.location;
  const query = url.href.split('?')[1] || '';
  const delimiter = '&';
  const result = {};

  const parts = query
  .split(delimiter);
  // TODO Step 3.3: Use Array.map() & Array.reduce()
  for (const i in parts) {
    const item = parts[i];
    const kv = item.split('=');
    result[kv[0]] = kv[1];
  }
  return result;

  /*
  const reducer = (accumulator, currentValue) => currentValue;
  return  parts.map( (p) =>  p.split('=') ).reduce(reducer);
  */

}
